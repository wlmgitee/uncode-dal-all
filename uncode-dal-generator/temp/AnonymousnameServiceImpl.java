package cn.uncode.dal.service.impl;

import cn.uncode.dal.dto.Anonymousname;
import cn.uncode.dal.service.AnonymousnameService;
import org.springframework.stereotype.Service;

import cn.uncode.dal.service.AbstractBaseService;

 /**
 * service类,此类由Uncode自动生成
 * @author uncode
 * @date 2017-02-21
 */
@Service
public class AnonymousnameServiceImpl extends AbstractBaseService<Anonymousname> implements AnonymousnameService {

}